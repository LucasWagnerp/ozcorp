/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public class Secretario extends Funcionario {

	public Secretario(String nome, String rg, String cpf, String matricula, String email, String senha,
			Sangue sangue, Sexo sexo, int nivelAcesso, Departamento departamento) {
		super(nome, rg, cpf, matricula, email, senha, sangue, sexo, nivelAcesso, departamento);
	
		
	}
}

