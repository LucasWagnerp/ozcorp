package br.com.ozcorp;

public enum Sangue {
	
	A_POS ("A_Positivo"),
	A_NEG ("A_Negativo"),
	AB_POS("AB_Positivo"),
	B_POS ("B_Positivo"),
	B_NEG ("B_Negativo"),
	AB_NEG("AB_Negativo"),
	O_POS ("O_Positivo"),
	O_NEG ("O_Negativo");
	
	public String sangue;
	
	Sangue(String sangue){
		this.sangue = sangue;
	}
}
