/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public class Departamento {

	String nome;
	String sigla;
	Cargo cargo;
	
	public Departamento(String nome, String sigla, Cargo cargo) {
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}

	// Getters & Setters
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		if(nome.length() > 3) {
		this.nome = nome;
		}else { 
			System.err.println("Nome inv�lido!");
		}
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}